## Inspiration
The recent rise of hate-crimes and Ann Morgan's TedTalk on reading a book from every country served as our inspiration for this project. 

Research has shown that cultural competency (having knowledge of other cultures, their practices, beliefs etc.) helps make us as a society more tolerant of our differences. However, achieving cultural competency is difficult. Some don't have the time, others don't have the resources needed to learn about other cultures. But after listening to Ann Morgan's TedTalk, we felt that stories can help us build some cultural cometency.

Stories are integral to our society, and humans have long communicated with each other through stories. They have the power to connect us through political, geographical, social, cultural, and regional divides; they transport us away from our reality and open our minds to different ideas, cultures, and people. They let us form connections with cultures we may never otherwise interact with. And this gives them the power to help reduce racism.

But most people never read stories outside of their cultural sphere. In fact, a large portion of books read in American schools about American society as it appears through the lens of white males (the authors).  And while some schools are trying to incorporate a more diverse set of stories into their curriculum, they haven't achieved a great deal of success as yet.

We wanted to change this. We wanted to make it easier. So, easy that anyone could engage with cultures around the world with just a tap of their fingers. Hence, we made 'Read The World'

## What it does
Read The World is a mobile application (for iOS and Android) that lets you read short stories from cultures around the world. We currently have stories from US, Mexico, India, and Africa, but we hope to add more countries soon. 

The app comes with some saved stories from various cultures as well as external links to websites that have more short stories.

## Installation Steps
1. Install Flutter on Android Studio/Xcode/Visual Studio
2. Create a new Flutter project
3. Follow installation steps for webview_flutter as mentioned here: https://pub.dev/packages/webview_flutter/install
4. Run pub get
5. Copy the two files in this repo and place them in /lib folder
6. Run the app on an emulator or a real phone