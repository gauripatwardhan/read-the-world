import 'Stories.dart';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:webview_flutter/webview_flutter.dart';

final Color darkBlue = Color.fromARGB(255, 18, 32, 47);

void main() {
  runApp(MyApp1());
}

class MyApp1 extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp1> {
  String _chosenValue = "American";

  List<Story> pickStorySet() {
    if (_chosenValue == "American") {
      return AmericanStory;
    }
    if (_chosenValue == 'African') {
      return AfricanStory;
    }
    if (_chosenValue == 'Indian') {
      return IndianStory;
    }
    return MexicanStory;
  }

  List<External> pickExternalSet() {
    if (_chosenValue == "American") {
      return AmericanExternalSources;
    }
    if (_chosenValue == 'African') {
      return AfricanExternalSources;
    }
    if (_chosenValue == 'Indian') {
      return IndianExternalSources;
    }
    return MexicanExternalSources;
  }


  Widget chooseLocation() {
    return DropdownButton<String>(
      focusColor: Colors.white,
      value: _chosenValue,
      //elevation: 5,
      style: TextStyle(color: Colors.white),
      iconEnabledColor: Colors.white,
      items: <String>[
        'American',
        'Indian',
        'Mexican',
        'African',
      ].map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(
            value,
            style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w500),
          ),
        );
      }).toList(),
      hint: Text(
        'American',
        style: TextStyle(
            color: Colors.white, fontSize: 18, fontWeight: FontWeight.w500),
      ),
      onChanged: (String? value) {
        if (value == null || _chosenValue == value) return;
        setState(() {
          _chosenValue = value;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark().copyWith(scaffoldBackgroundColor: darkBlue),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text('Stories'),
          actions: [
            chooseLocation(),
          ]
        ),
        body:
          DisplayStories(myStories: pickStorySet(), externalSources: pickExternalSet()),

      ),
    );
  }
}

class ExternalListItem extends StatelessWidget {
  final External externalSource;

  const ExternalListItem({Key? key, required this.externalSource})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    DisplayExternalSource(externalSource: externalSource)),
          );
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.grey,
          ),
          child: ListTile(
            title: Text(
              externalSource.title,
              style: TextStyle(
                color: Colors.black,
                fontSize: 18,
                fontFamily: "Open Sans Hebrew",
                fontWeight: FontWeight.w700,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_rounded,
                size: 26, color: Colors.black),
          ),
        ),
      ),
    );
  }
}

class DisplayExternalSource extends StatelessWidget {
  final External externalSource;

  const DisplayExternalSource({Key? key, required this.externalSource})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(externalSource.title),
      ),
      //passing in the ListView.builder
      body: WebView(
        initialUrl: externalSource.url,
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}

class DisplayMyStory extends StatelessWidget {
  final Story story;

  const DisplayMyStory({Key? key, required this.story}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(story.title),
      ),
      body: ListView(
          padding: const EdgeInsets.fromLTRB(16, 16, 16, 16),
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(story.imgUrl),
            ),
            SizedBox(height: 16),
            Text(story.content,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontFamily: 'Noto Sans CJK SC')),
          ]),
    );
  }
}

class MyStoryListItem extends StatelessWidget {
  final Story story;

  const MyStoryListItem({Key? key, required this.story}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DisplayMyStory(story: story)),
          );
        },
        child: Container(
          padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Color(0xff222444),
          ),
          child: ListTile(
            leading: ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.network(story.imgUrl),
            ),
            title: Text(
              story.title,
              style: TextStyle(
                color: Color(0xfffbfbfb),
                fontSize: 18,
                fontFamily: "Open Sans Hebrew",
                fontWeight: FontWeight.w700,
              ),
            ),
            trailing: Icon(Icons.arrow_forward_ios_rounded,
                size: 26, color: Colors.white),
          ),
        ),
      ),
    );
  }
}

class DisplayStories extends StatelessWidget {
  final List<Story> myStories;
  final List<External> externalSources;

  const DisplayStories(
      {Key? key, this.myStories = const [], this.externalSources = const []})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: myStories.length + externalSources.length,
        itemBuilder: (context, index) {
          if (index < myStories.length) {
            return MyStoryListItem(story: myStories[index]);
          } else {
            return ExternalListItem(
                externalSource: externalSources[index - myStories.length]);
          }
        },
      );
  }
}